FROM node:10.15.3-alpine

RUN mkdir -p /home/workspace/nuxt-static
WORKDIR /home/workspace/nuxt-static

# Set environment variables
ENV NODE_ENV development
ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 3000

# Bundle app source
COPY . /home/workspace/nuxt-static
RUN yarn build

EXPOSE 3000
CMD [ "yarn", "dev" ]