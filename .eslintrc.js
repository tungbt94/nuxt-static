module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'prettier',
    'plugin:vue/recommended',
    'prettier/vue',
    // 'plugin:prettier/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    'vue',
    'prettier',
  ],
  // add your custom rules here. 'off' ~ 0, 'warning' ~ 1, 'error' ~ 2
  rules: {
    'object-curly-spacing': ['error', 'always'],
    'indent': ['error', 2],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-trailing-spaces': 1,
    'vue/html-indent': 1,
    // 'vue/html-self-closing': 0,
    'vue/attributes-order': 1,
    'vue/require-default-prop': 1,
    'vue/max-attributes-per-line': ['error', { 'singleline': 3 }],
    'vue/attribute-hyphenation': 0,
    'vue/require-prop-types': 1,
    'vue/order-in-components': 1,
    'vue/mustache-interpolation-spacing': 0,
    'vue/html-self-closing': [
      'error',
      {
        'html': {
          'void': 'any'
        }
      }
    ],
    'semi': ["error", "never"]
  }
}
