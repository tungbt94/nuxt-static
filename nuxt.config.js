const pkg = require('./package')

const env = require('./config/env')

module.exports = {
  mode: 'spa',
  env,
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/lodash',
    '~/plugins/axios',
    '~/plugins/vuetify',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    'nuxt-purgecss',
    [
      'nuxt-i18n',
      {
        locales: [
          { code: 'en', iso: 'en_US', file: 'en.js', name: 'English' },
        ],
        defaultLocale: 'en',
        strategy: 'prefix_and_default',
        baseUrl: env.APP_URL,
        lazy: true,
        parsePages: false,
        langDir: 'lang/',
        vueI18n: {
          fallbackLocale: 'en',
        }
      }
    ],
  ],

  styleResources: {
    scss: []
  },

  purgeCSS: {
    styleExtensions: ['.css', '.scss']
  },
}
